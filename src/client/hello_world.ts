/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import {
  Keypair,
  Connection,
  PublicKey,
  LAMPORTS_PER_SOL,
  SystemProgram,
  TransactionInstruction,
  Transaction,
  sendAndConfirmTransaction,
  SYSVAR_RENT_PUBKEY,
} from '@solana/web3.js';
import fs from 'mz/fs';
import path from 'path';
import * as borsh from 'borsh';
//@ts-expect-error missing types
import * as BufferLayout from "buffer-layout";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const getRandomValues = require('get-random-values');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const BN = require('bn.js');

import {getPayer, getRpcUrl, createKeypairFromFile} from './utils';

/**
 * Connection to the network
 */
let connection: Connection;

/**
 * Keypair associated to the fees' payer
 */
let payer: Keypair;

/**
 * Hello world's program id
 */
let programId: PublicKey;

/**
 * The public key of the account we are saying hello to
 */
let greetedPubkey: PublicKey;

let scheduleKeypair: Keypair;

let pdaAccount: PublicKey;

let bump_seed;

/**
 * Path to program files
 */
const PROGRAM_PATH = path.resolve(__dirname, '../../dist/program');

/**
 * Path to program shared object file which should be deployed on chain.
 * This file is created when running either:
 *   - `npm run build:program-c`
 *   - `npm run build:program-rust`
 */
const PROGRAM_SO_PATH = path.join(PROGRAM_PATH, 'helloworld.so');

/**
 * Path to the keypair of the deployed program.
 * This file is created when running `solana program deploy dist/program/helloworld.so`
 */
const PROGRAM_KEYPAIR_PATH = path.join(PROGRAM_PATH, 'helloworld-keypair.json');

/**
 * The state of a greeting account managed by the hello world program
 */
class GreetingAccount {
  counter = 0;
  constructor(fields: {counter: number} | undefined = undefined) {
    if (fields) {
      this.counter = fields.counter;
    }
  }
}

class ScheduleAccount {
  release_time = 0;
  amount = 0;
  constructor(fields: {
    release_time: number,
    amount: number,
  } | undefined = undefined) {
    if (fields) {
      this.release_time = fields.release_time;
      this.amount = fields.amount;
    }
  }
}

/**
 * Borsh schema definition for greeting accounts
 */
const GreetingSchema = new Map([
  [GreetingAccount, {kind: 'struct', fields: [['counter', 'u32']]}],
]);

/**
 * Borsh schema definition for greeting accounts
 */
const ScheduleAccountSchema = new Map([
  [ScheduleAccount, {kind: 'struct', fields: [
    ['release_time', 'u64'],
    ['amount', 'u64'],
  ]}],
]);

/**
 * The expected size of each greeting account.
 */
const GREETING_SIZE = borsh.serialize(
  GreetingSchema,
  new GreetingAccount(),
).length;

let scheduleAccountBuffer: Uint8Array;

/**
 * Establish a connection to the cluster
 */
export async function establishConnection(): Promise<void> {
  const rpcUrl = await getRpcUrl();
  connection = new Connection(rpcUrl, 'confirmed');
  const version = await connection.getVersion();
  console.log('Connection to cluster established:', rpcUrl, version);
}

/**
 * Establish an account to pay for everything
 */
export async function establishPayer(): Promise<void> {
  if (!payer) {
    payer = await getPayer();
  }
}

/**
 * Check if the hello world BPF program has been deployed
 */
export async function checkProgram(): Promise<void> {
  // Read program id from keypair file
  try {
    const programKeypair = await createKeypairFromFile(PROGRAM_KEYPAIR_PATH);
    programId = programKeypair.publicKey;
  } catch (err) {
    const errMsg = (err as Error).message;
    throw new Error(
      `Failed to read program keypair at '${PROGRAM_KEYPAIR_PATH}' due to error: ${errMsg}. Program may need to be deployed with \`solana program deploy dist/program/helloworld.so\``,
    );
  }

  // Check if the program has been deployed
  const programInfo = await connection.getAccountInfo(programId);
  if (programInfo === null) {
    if (fs.existsSync(PROGRAM_SO_PATH)) {
      throw new Error(
        'Program needs to be deployed with `solana program deploy dist/program/helloworld.so`',
      );
    } else {
      throw new Error('Program needs to be built and deployed');
    }
  } else if (!programInfo.executable) {
    throw new Error(`Program is not executable`);
  }
  console.log(`Using program ${programId.toBase58()}`);
}

const publicKey = (property = "publicKey") => {
  return BufferLayout.blob(32, property);
};

const uint64 = (property = "uint64") => {
  return BufferLayout.blob(8, property);
};

export const ESCROW_ACCOUNT_DATA_LAYOUT = BufferLayout.struct([
  BufferLayout.u8("isInitialized"),
  publicKey("initializerPubkey"),
  publicKey("initializerTempTokenAccountPubkey"),
  publicKey("initializerReceivingTokenAccountPubkey"),
  uint64("expectedAmount"),
]);


// eslint-disable-next-line @typescript-eslint/require-await
export async function createScheduleAccount(): Promise<void> {
  scheduleKeypair = new Keypair();
  const signers = [payer, scheduleKeypair];
  const instruction = SystemProgram.createAccount({
    space: ESCROW_ACCOUNT_DATA_LAYOUT.span,
    lamports: await connection.getMinimumBalanceForRentExemption(
      ESCROW_ACCOUNT_DATA_LAYOUT.span
    ),
    fromPubkey: payer.publicKey,
    newAccountPubkey: scheduleKeypair.publicKey,
    programId: programId,
  });
  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    signers,
  ).then(function(res){
    console.log(res);
  });
}

class Assignable
{
    [index:string]:any;
    constructor(properties:{[index:string]:any}) 
    {
        Object.keys(properties).map((key:string) => {
            this[key] = properties[key];
        });
    }
}
class InitAccountData extends Assignable
{
    seedStr?:string;
    numberOfSchedules?:number;
    constructor(properties:{[index:string]:any})
    {
        super(properties);
    }
}
const InitAccountDataSchema = new Map([
    [InitAccountData, { 
        kind: 'struct', 
        fields: [ 
            ['seedStr', 'string'],
            ['numberOfSchedules', 'u32'], //adding this property it
        ] 
    }],
]);

const schedules = [
  new ScheduleAccount({
    release_time: 1633611454,
    amount: 2,
  }),
  new ScheduleAccount({
    release_time: 1633611454,
    amount: 73,
  }),
];

// eslint-disable-next-line @typescript-eslint/require-await
export async function initAccount(): Promise<void> {
  const seedStr = 'lL0loPG6T8$*J56!D3szgv%sCeWmQ6uZ';
  const numberOfSchedules = schedules.length;
  const initAccountData:InitAccountData = new InitAccountData({seedStr, numberOfSchedules})
  const initAccountDataSerialized = borsh.serialize(InitAccountDataSchema, initAccountData);
 
  const seeds = [ Buffer.from( seedStr ) ];
  [ pdaAccount, bump_seed ] = await PublicKey.findProgramAddress(seeds, programId); 

  // check account to see if it already exists...
  const pdaAccountInfo = await connection.getAccountInfo(pdaAccount);

  if ( pdaAccountInfo?.owner.toBase58() == programId.toBase58() )
    return console.log("account already owned by program, skipping init account");

  if ( pdaAccountInfo?.owner != null && pdaAccountInfo?.owner.toBase58() != programId.toBase58() )
    throw new Error(
      `PDA account is already created and is not owned by the program`,
    );

  const signers = [payer];
  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: SystemProgram.programId, isSigner: false, isWritable: false},
      {pubkey: SYSVAR_RENT_PUBKEY, isSigner: false, isWritable: false},
      {pubkey: payer.publicKey, isSigner: false, isWritable: false},
      {pubkey: pdaAccount, isSigner: false, isWritable: true},
    ],
    programId,
    data: Buffer.from(
      Uint8Array.of(0, ...initAccountDataSerialized)
    ),
  });
  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    signers,
  ).then(function(res){
    console.log(res);
  });
}

function serializeArray(array: Array<any>, Schema: Map<any, any>) {
    let resultBuffer: Uint8Array = new Uint8Array();

    for ( let index = 0; index < array.length; index++ ) {
        resultBuffer = Uint8Array.of(
              ...resultBuffer, 
              ...borsh.serialize(
                  Schema,
                  array[ index ]     
              )
        );
    }

    return resultBuffer;
}

// eslint-disable-next-line @typescript-eslint/require-await
export async function create(): Promise<void> {
  const scheduleAccountBuffer = serializeArray(schedules, ScheduleAccountSchema);
  const signers = [payer];
  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: pdaAccount, isSigner: false, isWritable: true},
    ],
    programId,
    data: Buffer.from(
      Uint8Array.of(1, ...scheduleAccountBuffer)
    ),
  });
  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    signers,
  ).then(function(res){
    console.log(res);
  });
}


// eslint-disable-next-line @typescript-eslint/require-await
export async function printAccountData(): Promise<void> {
  const signers = [payer];
  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: pdaAccount, isSigner: false, isWritable: true},
    ],
    programId,
    data: Buffer.from(
      Uint8Array.of(2)
    ),
  });
  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    signers,
  ).then(function(res){
    console.log(res);
  });
}

