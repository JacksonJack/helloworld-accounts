/**
 * Hello world
 */

import {
  establishConnection,
  establishPayer,
  checkProgram,
  initAccount,
  printAccountData,
  create,
} from './hello_world';

async function main() {
  console.log("Let's say hello to a Solana account...");

  // Establish connection to the cluster
  await establishConnection();

  await establishPayer();

  // Check if the program has been deployed
  await checkProgram();

  // init account
  await initAccount();

  // create the contract 
  await create();

  // print account data to the transaction log
  await printAccountData();

  console.log('Success');
}

main().then(
  () => process.exit(),
  err => {
    console.error(err);
    process.exit(-1);
  },
);
