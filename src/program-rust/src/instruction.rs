use solana_program::program_error::ProgramError;
use solana_program::msg;
use std::convert::TryInto;
use crate::error::AccountError::InvalidInstruction;
use std::str;
use borsh::{BorshSerialize, BorshDeserialize};

pub struct Schedule {
    pub release_time: u64,
    pub amount: u64,
}

pub enum AccountInstruction {
    InitAccount {
        seed_str: String,
        number_of_schedules: u32,
    },
    Create {
        schedules: Vec<Schedule>,
    },
    PrintAccountData {},
}

pub const SCHEDULE_SIZE: usize = 16;

#[derive(BorshSerialize, BorshDeserialize, PartialEq, Debug)]
struct InitAccountData {
    seed_str: String,
    number_of_schedules: u32,
}

impl AccountInstruction {
    pub fn unpack(input: &[u8]) -> Result<Self, ProgramError> {
        let (tag, rest) = input.split_first().ok_or(InvalidInstruction)?;
      
        Ok(match tag {
            0 => {
                let decoded_data = InitAccountData::try_from_slice(&rest).unwrap();
                msg!("decoded_data.seed_str: {}", decoded_data.seed_str);
                msg!("decoded_data.number_of_schedules: {}", decoded_data.number_of_schedules);
                Self::InitAccount {
                    seed_str: decoded_data.seed_str,
                    number_of_schedules: decoded_data.number_of_schedules,
                }
            }
            1 => {
                let number_of_schedules = rest[0..].len() / SCHEDULE_SIZE;
                let mut schedules: Vec<Schedule> = Vec::with_capacity(number_of_schedules);
                let mut offset = 0;
                for _ in 0..number_of_schedules {
                    let release_time = rest
                        .get(offset..offset + 8)
                        .and_then(|slice| slice.try_into().ok())
                        .map(u64::from_le_bytes)
                        .ok_or(InvalidInstruction)?;
                    let amount = rest
                        .get(offset + 8..offset + 16)
                        .and_then(|slice| slice.try_into().ok())
                        .map(u64::from_le_bytes)
                        .ok_or(InvalidInstruction)?;
                    offset += SCHEDULE_SIZE;
                    schedules.push(Schedule {
                        release_time,
                        amount,
                    })
                }
                Self::Create {
                  schedules,
                }
            },
            2 => Self::PrintAccountData {},
            _ => return Err(InvalidInstruction.into()),
        })
    }
}