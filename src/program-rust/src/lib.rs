pub mod error;
pub mod instruction;
pub mod state;

use std::str;
use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::{
    account_info::{AccountInfo, next_account_info},
    entrypoint,
    entrypoint::ProgramResult,
    msg,
    program::{invoke, invoke_signed},
    program_error::ProgramError,
    program_pack::{IsInitialized, Pack, Sealed},
    pubkey::Pubkey,
    rent::Rent,
    system_instruction::create_account,
    sysvar::Sysvar
};
use crate::{
  instruction::{Schedule, AccountInstruction, SCHEDULE_SIZE},
  state::{VestingSchedule, unpack_schedules}, 
};


/// Define the type of state stored in accounts
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct GreetingAccount {
    /// number of greetings
    pub counter: u32,
}

// Declare and export the program's entrypoint
entrypoint!(process_instruction);

// Program entrypoint's implementation
pub fn process_instruction(
    program_id: &Pubkey, 
    accounts: &[AccountInfo], 
    instruction_data: &[u8],
) -> ProgramResult {
  let instruction = AccountInstruction::unpack(instruction_data)?;

  match instruction {
      AccountInstruction::InitAccount { seed_str, number_of_schedules } => {
          msg!("Instruction: InitAccount");
          process_init_account(accounts, program_id, seed_str, number_of_schedules)
      }
      AccountInstruction::Create { schedules } => {
          msg!("Instruction: Create");
          process_create(accounts, program_id, schedules)
      }
      AccountInstruction::PrintAccountData {} => {
          msg!("Instruction: PrintAccountData");
          process_print_account_data(accounts, program_id)
      }
  }
}

fn process_init_account(
  accounts: &[AccountInfo],
  program_id: &Pubkey,
  seed_str: String,
  number_of_schedules: u32,
) -> ProgramResult {
    let accounts_iter = &mut accounts.iter();

    let system_program_account = next_account_info(accounts_iter)?;
    let rent = &Rent::from_account_info(next_account_info(accounts_iter)?)?;
    let payer = next_account_info(accounts_iter)?;
    let pda_account = next_account_info(accounts_iter)?;

    // Find the non reversible public key for the vesting contract via the seed    
    let (pda_account_key, bump_seed) = get_associated_token_address_and_bump_seed_internal(
      &seed_str,
      program_id,
    );

    msg!("pda_account_key: {}", pda_account_key);
    msg!("pda_account.key: {}", pda_account.key);

    if pda_account_key != *pda_account.key {
        msg!("Provided pda account is invalid");
        return Err(ProgramError::InvalidArgument);
    }

    let state_size = (number_of_schedules as usize) * SCHEDULE_SIZE;

    let init_pda_account = create_account(
        &payer.key,
        &pda_account_key,
        rent.minimum_balance(state_size),
        state_size as u64,
        &program_id,
    );

    let associated_token_account_signer_seeds: &[&[_]] = &[
        &seed_str.as_bytes(),
        &[bump_seed],
    ];

    invoke_signed(
        &init_pda_account,
        &[
            system_program_account.clone(),
            payer.clone(),
            pda_account.clone(),
        ],
        &[associated_token_account_signer_seeds]
    )?;
    Ok(())
}

fn process_create(
  accounts: &[AccountInfo],
  program_id: &Pubkey,
  schedules: Vec<Schedule>,
) -> ProgramResult {
    let accounts_iter = &mut accounts.iter();

    let pda_account = next_account_info(accounts_iter)?;

    let mut data = pda_account.data.borrow_mut();
    if data.len() != schedules.len() * VestingSchedule::LEN {
        return Err(ProgramError::InvalidAccountData)
    }

    let mut offset = 0;

    for s in schedules.iter() {
        let state_schedule = VestingSchedule {
            release_time: s.release_time,
            amount: s.amount,
        };
        state_schedule.pack_into_slice(&mut data[offset..]);
        offset += SCHEDULE_SIZE;
    }

    Ok(())
}

fn process_print_account_data(
  accounts: &[AccountInfo],
  program_id: &Pubkey,
) -> ProgramResult {
    let accounts_iter = &mut accounts.iter();

    let pda_account = next_account_info(accounts_iter)?;

    let mut schedules = unpack_schedules(&pda_account.data.borrow())?;

    for s in schedules.iter() {
        msg!("s.release_time {}", s.release_time);
        msg!("s.amount {}", s.amount);
    };

    Ok(())
}

fn get_associated_token_address_and_bump_seed_internal(
  seed_string: &str,
  program_id: &Pubkey,
) -> (Pubkey, u8) {
  Pubkey::find_program_address(
      &[
          &seed_string.as_bytes(),
      ],
      program_id,
  )
}