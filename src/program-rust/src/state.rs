use solana_program::{
  program_error::ProgramError,
  program_pack::{IsInitialized, Pack, Sealed},
  pubkey::Pubkey,
};

use std::convert::TryInto;
#[derive(Debug, PartialEq)]
pub struct VestingSchedule {
  pub release_time: u64,
  pub amount: u64,
}

impl Sealed for VestingSchedule {}

impl Pack for VestingSchedule {
  const LEN: usize = 16;

  fn pack_into_slice(&self, dst: &mut [u8]) {
      let release_time_bytes = self.release_time.to_le_bytes();
      let amount_bytes = self.amount.to_le_bytes();
      for i in 0..8 {
          dst[i] = release_time_bytes[i];
      }

      for i in 8..16 {
          dst[i] = amount_bytes[i - 8];
      }
  }

  fn unpack_from_slice(src: &[u8]) -> Result<Self, ProgramError> {
      if src.len() < 16 {
          return Err(ProgramError::InvalidAccountData)
      }
      let release_time = u64::from_le_bytes(src[0..8].try_into().unwrap());
      let amount = u64::from_le_bytes(src[8..16].try_into().unwrap());
      Ok(Self {
          release_time,
          amount,
      })
  }
}

pub fn unpack_schedules(input: &[u8]) -> Result<Vec<VestingSchedule>, ProgramError> {
  let number_of_schedules = input.len() / VestingSchedule::LEN;
  let mut output: Vec<VestingSchedule> = Vec::with_capacity(number_of_schedules);
  let mut offset = 0;
  for _ in 0..number_of_schedules {
      output.push(VestingSchedule::unpack_from_slice(
          &input[offset..offset + VestingSchedule::LEN],
      )?);
      offset += VestingSchedule::LEN;
  }
  Ok(output)
}

pub fn pack_schedules_into_slice(schedules: Vec<VestingSchedule>, target: &mut [u8]) {
  let mut offset = 0;
  for s in schedules.iter() {
      s.pack_into_slice(&mut target[offset..]);
      offset += VestingSchedule::LEN;
  }
}
